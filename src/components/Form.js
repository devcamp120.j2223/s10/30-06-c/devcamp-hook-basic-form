import { useEffect, useState } from "react";

function Form() {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstname") || "");
    const [lastName, setLastName] = useState(localStorage.getItem("lastname") || "");

    const firstNameChangeHandler = (event) => {
        setFirstName(event.target.value);
    }

    const lastNameChangeHandler = (event) => {
        setLastName(event.target.value);
    }

    useEffect(() => {
        localStorage.setItem("firstname", firstName);
        localStorage.setItem("lastname", lastName);
    })

    useEffect(() => {
       document.title = "Basic form"
    }, [])

    return (
        <div>
            <div><input value={firstName} onChange={firstNameChangeHandler} placeholder="Firstname"/></div>
            <div><input value={lastName} onChange={lastNameChangeHandler} placeholder="Lastname"/></div>
            <p>{lastName} {firstName}</p>
        </div>
    )
}

export default Form;